package GameObject;

import java.awt.Graphics;

import Core.Handler;
import Graphics.Assets;
import World.Tile;

public class Rune extends StaticObject{
	public static final int HP_RUNE = 0;
	public static final int MP_RUNE = 1;
	public static final int RESTORE_RUNE = 2;

	private int runeType;
	private boolean eaten;
	
	//Constructors
	public Rune(Handler handler, int runeType, int x, int y) {
		super(handler, x, y, Tile.TILE_WIDTH, Tile.TILE_HEIGHT);
		if (runeType >= 0 || runeType <= 2) this.runeType = runeType;
		eaten = false;
	}

	//Methods
	@Override
	public void update() {
		
	}

	@Override
	public void render(Graphics g) {
		int xOffset = handler.getGame().getGameCamera().getxOffset();
		int yOffset = handler.getGame().getGameCamera().getyOffset();
		switch (runeType) {
		case HP_RUNE:
			g.drawImage(Assets.hp_rune, x - xOffset, y - yOffset, 32, 32, null);break;
		case MP_RUNE:
			g.drawImage(Assets.mp_rune, x - xOffset, y - yOffset, 32, 32, null);break;
		case RESTORE_RUNE:
			g.drawImage(Assets.regen_rune, x - xOffset, y - yOffset, 32, 32, null);break;
		default:
			break;
		}
	}
	
	//Getters and Setters
	public int getRuneType() {
		return runeType;
	}

	public boolean isEaten() {
		return eaten;
	}

	public void setEaten(boolean eaten) {
		this.eaten = eaten;
	}
}
