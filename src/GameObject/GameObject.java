package GameObject;

import java.awt.Graphics;
import java.awt.Rectangle;

import Core.Handler;

public abstract class GameObject {
	
	protected Handler handler;
	protected int x,y;
	protected int width, height;
	protected Rectangle collisionBound;
	
	//Constructors
	public GameObject(Handler handler, int x, int y, int width, int height) {
		this.handler = handler;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		collisionBound = new Rectangle(0, 0, width, height);
	}
	
	//Methods	
	public Rectangle getCollisionBound(int xOffset, int yOffset) {
		return new Rectangle(x+collisionBound.x+xOffset, y+collisionBound.y+yOffset, 
				collisionBound.width, collisionBound.height);
	}
	
	public abstract void update();
	public abstract void render(Graphics g);

	//Getters and Setters

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
}
