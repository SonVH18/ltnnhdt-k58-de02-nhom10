package GameObject;

public class Skill {
	public static final int NUKE = 1;
	public static final int PHYSICAL = 2;
	public static final int BUFF_DEFENSE = 3;
	public static final int BUFF_ATTACK = 4;
	
	private int damage;
	private int skillType;
	private int manaCost;
	
	//Constructors
	public Skill(int damage, int skillType, int manaCost) {
		setDamage(damage);
		setSkillType(skillType);
		setManaCost(manaCost);
	}

	//getters and Setters
	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		if (damage >= 0) this.damage = damage;
	}

	public int getSkillType() {
		return skillType;
	}

	public void setSkillType(int skillType) {
		if (skillType > 0 || skillType < 5) this.skillType = skillType;
	}

	public int getManaCost() {
		return manaCost;
	}

	public void setManaCost(int manaCost) {
		if (manaCost > 0) this.manaCost = manaCost;
	}
	
	
}
