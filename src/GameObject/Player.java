package GameObject;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import Core.Handler;
import Graphics.Animation;
import Graphics.Assets;
import World.Tile;

public class Player extends DynamicObject{
	
	private int MP, maxMP, normalAtk, normalDef;
	private boolean buffAtk, buffDef;
	private Animation animUp, animDown, animLeft, animRight;
	private Skill[] skills;

	//Constructors
	private Player(Handler handler, int x, int y) {
		super(handler, x, y, Tile.TILE_WIDTH, Tile.TILE_HEIGHT);
		collisionBound.x = 8;
		collisionBound.y = 16;
		collisionBound.width = 15;
		collisionBound.height = 15;
		
		animUp = new Animation(150, Assets.p_up);
		animDown = new Animation(150, Assets.p_down);
		animLeft = new Animation(150, Assets.p_left);
		animRight = new Animation(150, Assets.p_right);
		
		this.atk = 2 * DEFAULT_ATK;
		this.def = DEFAULT_DEF;
		this.HP = 8 * DEFAULT_HP;
		this.maxHP = this.HP;
		this.MP = 500;
		this.maxMP = this.MP;
		buffAtk = false;
		buffDef = false;
		normalAtk = this.atk;
		normalDef = this.def;
		skills = new Skill[4];
		skills[0] = new Skill(this.atk, Skill.PHYSICAL, 0);
		skills[1] = new Skill(300, Skill.NUKE, 200);
		skills[2] = new Skill(100, Skill.BUFF_ATTACK, 100);
		skills[3] = new Skill(50, Skill.BUFF_DEFENSE, 100);
	}
	
	//Methods
	private void getInput() {
		xMove = 0;
		yMove = 0;
		if (handler.getGame().getKeyInput().up)
			yMove = -speed;
		else if (handler.getGame().getKeyInput().down)
			yMove = speed;
		else if (handler.getGame().getKeyInput().left)
			xMove = -speed;
		else if (handler.getGame().getKeyInput().right)
			xMove = speed;
	}
	
	public int checkGateCollision() {
		for (Gate g : handler.getGameObjectManager().getGate()) {
			if (g.getCollisionBound(0, 0).intersects(getCollisionBound(0, 0)))
				return g.getGateType();
		}
		return -1;
	}
	
	public Rune checkRuneCollision() {
		for (Rune r : handler.getGameObjectManager().getRune()) {
			if (r.getCollisionBound(0, 0).intersects(getCollisionBound(0, 0)))
				return r;
		}
		return null;
	}
	
	public Enemy checkEnemyCollision(int xOffset, int yOffset) {
		for (Enemy e : handler.getGameObjectManager().getEnemy()) {
			if (!e.getDeadStatus())
				if (e.getCollisionBound(0, 0).intersects(getCollisionBound(xOffset, yOffset))) {
				return e;
			}
		}
		return null;
	}
	

	public void drainMana(int manaCost) {
		this.setMP(this.getMP()-manaCost);
	}
	
	public void eatRune(Rune rune) {
		if (rune.isEaten()) return;
		switch (rune.getRuneType()) {
		case Rune.HP_RUNE:
			setHP(maxHP);
			break;
		case Rune.MP_RUNE:
			setMP(maxMP);
			break;
		case Rune.RESTORE_RUNE:
			setHP(maxHP);
			setMP(maxMP);
			break;
		default:break;
		}
		rune.setEaten(true);
	}
	
	public void animUpdate() {
		animUp.update();
		animDown.update();
		animLeft.update();
		animRight.update();
	}

	@Override
	public void update() {
		animUpdate();
		getInput();
		move();
		handler.getGame().getGameCamera().centerOnGameObject(this);
	}

	@Override
	public void render(Graphics g) {
		int xOffset = handler.getGame().getGameCamera().getxOffset();
		int yOffset = handler.getGame().getGameCamera().getyOffset();
		g.drawImage(getCurrentAnimationFrame(), x - xOffset, y - yOffset, width, height, null);
		//Draw collision bound
		//g.setColor(Color.RED);
		//g.fillRect(x+collisionBound.x-xOffset, y+collisionBound.y-yOffset, collisionBound.width, collisionBound.height);
	}
	
	private BufferedImage getCurrentAnimationFrame() {
		if (xMove > 0) return animRight.getFrame();
		else if (xMove < 0) return animLeft.getFrame();
		else if (yMove > 0) return animDown.getFrame();
		else if (yMove < 0) return animUp.getFrame();
		else return Assets.p_down[1];
	}
	public BufferedImage getBattleAnimationFrame() {
		return animRight.getFrame();
	}

	public void resetStat() {
		setHP(maxHP);
		setMP(maxMP);
		setAtk(normalAtk);
		setDef(normalDef);
		setBuffAtk(false);
		setBuffDef(false);
	}
	
	//Getters and Setters
	public Skill[] getSkills() {
		return skills;
	}

	public int getMP() {
		return MP;
	}

	public void setMP(int mP) {
		MP = mP;
	}

	public int getMaxMP() {
		return maxMP;
	}
	

	public void setMaxMP(int maxMP) {
		this.maxMP = maxMP;
	}

	public boolean isBuffAtk() {
		return buffAtk;
	}

	public void setBuffAtk(boolean buffAtk) {
		this.buffAtk = buffAtk;
	}

	public boolean isBuffDef() {
		return buffDef;
	}

	public void setBuffDef(boolean buffDef) {
		this.buffDef = buffDef;
	}

	public int getNormalAtk() {
		return normalAtk;
	}

	public int getNormalDef() {
		return normalDef;
	}

	//Singleton
	private static Player inst;
	public static Player getInst(Handler handler) {
		if (inst == null)
			inst = new Player(handler, 0, 0);
		return inst;
	}

}
