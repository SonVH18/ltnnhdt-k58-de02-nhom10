package GameObject;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import Core.Handler;
import Graphics.Assets;
import World.Tile;

public class Gate extends StaticObject{
	public static final int NEXT_MAP_GATE = 0;
	public static final int PREV_MAP_GATE = 1;
	public static final int END_GAME_GATE = 2;
	
	private int gateType;

	//Constructors
	public Gate(Handler handler, int gateType, int x, int y) {
		super(handler, x, y, Tile.TILE_WIDTH, Tile.TILE_HEIGHT);
		if (gateType >= 0 || gateType <= 2) this.gateType = gateType;
		collisionBound.x = 0;
		collisionBound.y = 0;
		collisionBound.width = 32;
		collisionBound.height = 32;		
	}

	//Methods
	@Override
	public void update() {
		
	}

	@Override
	public void render(Graphics g) {
		int xOffset = handler.getGame().getGameCamera().getxOffset();
		int yOffset = handler.getGame().getGameCamera().getyOffset();
		BufferedImage image;
		if (gateType == Gate.END_GAME_GATE) image = Assets.flag;
		else image = Assets.gate;
		g.drawImage(image, x-xOffset, y-yOffset, width, height, null);
	}

	//Getters and Setters
	public int getGateType() {
		return gateType;
	}
	
	

}
