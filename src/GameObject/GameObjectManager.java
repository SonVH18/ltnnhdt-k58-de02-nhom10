package GameObject;

import java.awt.Graphics;
import java.util.ArrayList;

import Core.Handler;

public class GameObjectManager {
	private Handler handler;
	private Player player;
	private Enemy[] enemy;
	private Gate[] gate;
	private Rune[] rune;
	private ArrayList<GameObject> gameObjects;
	
	//Constructors	
	private GameObjectManager(Handler handler) {
		this.handler = handler;
		gameObjects = new ArrayList<GameObject>();
	}
	
	//Methods
	public void update() {
		for (GameObject obj : gameObjects) 
			obj.update();
		player.update();
	}
	
	public void render(Graphics g) {
		for (GameObject obj : gameObjects) 
			obj.render(g);
		player.render(g);
	}
	
	public void addPlayer(Player player) {
		this.setPlayer(player);
		addObject(player);
	}
	
	public void addEnemy(Enemy[] enemy) {
		this.setEnemy(enemy);
		for (Enemy e : enemy) addObject(e);
	}
	
	public void addGate(Gate[] gate) {
		this.setGate(gate);
		for (Gate g : gate) addObject(g);
	}
	
	public void addRune(Rune[] rune) {
		this.setRune(rune);
		for (Rune e : rune) addObject(e);
	}
	
	public void addObject(GameObject obj) {
		gameObjects.add(obj);
	}
	
	public void clear() {
		player = null;
		enemy = null;
		gate = null;
		rune = null;
		gameObjects.clear();
	}

	//Getters and Setters
	public Handler getHandler() {
		return handler;
	}

	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public ArrayList<GameObject> getGameObjects() {
		return gameObjects;
	}

	public void setGameObjects(ArrayList<GameObject> gameObjects) {
		this.gameObjects = gameObjects;
	}

	public Enemy[] getEnemy() {
		return enemy;
	}

	public void setEnemy(Enemy[] enemy) {
		this.enemy = enemy;
	}

	public Gate[] getGate() {
		return gate;
	}

	public void setGate(Gate[] gate) {
		this.gate = gate;
	}

	public Rune[] getRune() {
		return rune;
	}

	public void setRune(Rune[] rune) {
		this.rune = rune;
	}
	
	//Singleton
	private static GameObjectManager inst;
	public static GameObjectManager getInst(Handler handler) {
		if (inst == null) 
			inst = new GameObjectManager(handler);
		return inst;
	}
	
}
