package GameObject;

import Core.Handler;
import World.Tile;

public abstract class DynamicObject extends GameObject{
	public static final int DEFAULT_HP = 100;
	public static final int DEFAULT_SPEED = 3;
	public static final int DEFAULT_ATK = 50;
	public static final int DEFAULT_DEF = 50;
	protected int HP, speed, atk, def, maxHP;
	protected int xMove, yMove;
	protected boolean isDead;

	//Constructors
	public DynamicObject(Handler handler, int x, int y, int width, int height) {
		super(handler, x, y, width, height);
		HP = DEFAULT_HP;
		maxHP = DEFAULT_HP;
		speed = DEFAULT_SPEED;
		atk = DEFAULT_ATK;
		def = DEFAULT_DEF;
		xMove = 0;
		yMove = 0;
		isDead = false;
	}
	
	//Methods
	public void move() {
		moveX();
		moveY();		
	}
	
	private void moveX() {		
		if (xMove > 0) {//Move right
			int tempX = (x + xMove + collisionBound.x + collisionBound.width) / Tile.TILE_WIDTH;
			if (!checkCollisionWithTile(tempX, (int) (y+collisionBound.y) / Tile.TILE_HEIGHT) &&
					!checkCollisionWithTile(tempX, (y+collisionBound.y+collisionBound.height) / Tile.TILE_HEIGHT)) {
				x += xMove;
			} else {
				x = tempX * Tile.TILE_WIDTH - collisionBound.x - collisionBound.width - 1;
			}
			
		} else if (xMove < 0) {//Move left
			int tempX = (x + xMove + collisionBound.x) / Tile.TILE_WIDTH;
			if (!checkCollisionWithTile(tempX, (int) (y+collisionBound.y) / Tile.TILE_HEIGHT) &&
					!checkCollisionWithTile(tempX, (y+collisionBound.y+collisionBound.height) / Tile.TILE_HEIGHT)) {
				x += xMove;
			} else {
				x = tempX * Tile.TILE_WIDTH + Tile.TILE_WIDTH - collisionBound.x;
			}
		}
	}
	
	private void moveY() {
		
		if (yMove > 0) {//Move down
			int tempY = (y + yMove + collisionBound.y + collisionBound.height) / Tile.TILE_HEIGHT;
			if (!checkCollisionWithTile((int) (x+collisionBound.x) / Tile.TILE_WIDTH, tempY) &&
					!checkCollisionWithTile((x+collisionBound.x+collisionBound.width) / Tile.TILE_WIDTH, tempY)) {
				y += yMove;
			} else {
				y = tempY * Tile.TILE_HEIGHT - collisionBound.y - collisionBound.height - 1;
			}
			
		} else if (yMove < 0) {// Move Up
			int tempY = (y + yMove + collisionBound.y) / Tile.TILE_HEIGHT;
			if (!checkCollisionWithTile((int) (x+collisionBound.x) / Tile.TILE_WIDTH, tempY) &&
					!checkCollisionWithTile((x+collisionBound.x+collisionBound.width) / Tile.TILE_WIDTH, tempY)) {
				y += yMove;
			} else {
				y = tempY * Tile.TILE_HEIGHT + Tile.TILE_HEIGHT - collisionBound.y;
			}
		}
	}
	
	protected boolean checkCollisionWithTile(int x, int y) {
		return handler.getWorld().getTile(x, y).isSolid();
	}
	
	public void takeDamage(int damage) {
		this.HP -= damage;
		if (HP <= 0) {
			HP = 0;
			kill();
		}
	}
	
	//Getters and Setters

	public int getHP() {
		return HP;
	}

	public void setHP(int hP) {
		HP = hP;
	}

	public int getxMove() {
		return xMove;
	}

	public void setxMove(int xMove) {
		this.xMove = xMove;
	}

	public int getyMove() {
		return yMove;
	}

	public void setyMove(int yMove) {
		this.yMove = yMove;
	}

	public int getAtk() {
		return atk;
	}

	public void setAtk(int atk) {
		this.atk = atk;
	}

	public int getDef() {
		return def;
	}

	public void setDef(int def) {
		this.def = def;
	}

	public int getMaxHP() {
		return maxHP;
	}

	public void setMaxHP(int maxHP) {
		this.maxHP = maxHP;
	}
	
	public boolean getDeadStatus() {
		return isDead;
	}
	
	public void kill() {
		isDead = true;
	}

}
