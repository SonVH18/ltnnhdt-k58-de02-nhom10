package GameObject;

import Core.Handler;

public abstract class StaticObject extends GameObject{

	public StaticObject(Handler handler, int x, int y, int width, int height) {
		super(handler, x, y, width, height);
	}

}
