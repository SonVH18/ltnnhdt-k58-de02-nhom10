package GameObject;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import Core.Handler;
import Graphics.Animation;
import Graphics.Assets;
import Utils.RandomInt;
import World.Tile;

public class Enemy extends DynamicObject{
	private Animation animUp, animDown, animLeft, animRight;
	private Point[] patrolRoute;
	private int index;

	//Constructors
	public Enemy(Handler handler, int x, int y, Point[] patrolRoute) {
		super(handler, x, y, Tile.TILE_WIDTH, Tile.TILE_HEIGHT);
		this.patrolRoute = patrolRoute;
		this.index = 0;
		this.speed = 1;
		
		collisionBound.x = 8;
		collisionBound.y = 16;
		collisionBound.width = 15;
		collisionBound.height = 15;
		
		animUp = new Animation(150, Assets.orc_up);
		animDown = new Animation(150, Assets.orc_down);
		animLeft = new Animation(150, Assets.orc_left);
		animRight = new Animation(150, Assets.orc_right);
		
		genStat();
	}
	
	//Methods
	public void genStat() {
		atk = DEFAULT_ATK * (RandomInt.generate(3)+1);
		def = DEFAULT_DEF * RandomInt.generate(1);
		maxHP = DEFAULT_HP *( RandomInt.generate(2)+1);
		HP = maxHP;
	}
	
	public void patrol() {		
		int nextIndex = index+1;
		if (patrolRoute[nextIndex] == null) nextIndex = 0;
		
		xMove = 0;
		yMove = 0;
		if (patrolRoute[index].getX() == patrolRoute[nextIndex].getX()) {
			if (patrolRoute[index].getY() < patrolRoute[nextIndex].getY()) 
				yMove = speed;
			else if (patrolRoute[index].getY() > patrolRoute[nextIndex].getY()) 
				yMove = -speed;
		}
		if (patrolRoute[index].getY() == patrolRoute[nextIndex].getY()) {
			if (patrolRoute[index].getX() < patrolRoute[nextIndex].getX()) 
				xMove = speed;
			else if (patrolRoute[index].getX() > patrolRoute[nextIndex].getX()) 
				xMove = -speed;
		}
		move();
		if (checkCurrentPosition()) index++;
		if (patrolRoute[index] == null) index = 0;		
	}
	
	private boolean checkCurrentPosition() {		
		for (int i=0; i<patrolRoute.length;i++) {
			if (patrolRoute[i]==null) return false;
			if (this.x == patrolRoute[i].getX() && this.y == patrolRoute[i].getY())
				return true;
		}
		return false;
	}

	@Override
	public void update() {
		animUpdate();
		patrol();
	}

	@Override
	public void render(Graphics g) {
		int xOffset = handler.getGame().getGameCamera().getxOffset();
		int yOffset = handler.getGame().getGameCamera().getyOffset();
		g.drawImage(getCurrentAnimationFrame(), x - xOffset, y - yOffset, width, height, null);
	}
	
	private BufferedImage getCurrentAnimationFrame() {
		if (xMove > 0) return animRight.getFrame();
		else if (xMove < 0) return animLeft.getFrame();
		else if (yMove > 0) return animDown.getFrame();
		else if (yMove < 0) return animUp.getFrame();
		else return Assets.orc_down[1];
	}
	
	public BufferedImage getBattleAnimationFrame() {
		return animLeft.getFrame();
	}

	public void animUpdate() {
		animUp.update();
		animDown.update();
		animLeft.update();
		animRight.update();
	}
	
	

}
