package World;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Tile {
	public static final int TILE_WIDTH = 32, TILE_HEIGHT = 32;
	public static Tile[] tiles = new Tile[256];
	public static Tile grassTile = new GrassTile(0);
	public static Tile stoneTile = new StoneTile(1);
	
	//Constructors
	protected BufferedImage texture;
	protected final int id; 
	protected boolean solid;
	public Tile(BufferedImage texture, int id) {
		this.texture = texture;
		this.id = id;
		tiles[id] = this;
		solid = false;
	}
	
	//Methods
	public void update() {
		
	}
	
	public void render(Graphics g, int x, int y) {
		g.drawImage(texture, x, y, TILE_WIDTH, TILE_HEIGHT, null);
	}
	
	//Getters and Setters
	public int getID() {
		return id;
	}
	
	public boolean isSolid() {
		return solid;
	}
}
