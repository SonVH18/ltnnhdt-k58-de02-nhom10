package World;

import Graphics.Assets;

public class StoneTile extends Tile {

	public StoneTile(int id) {
		super(Assets.rock, id);
		solid = true;
	}

}
