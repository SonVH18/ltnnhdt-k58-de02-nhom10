package World;

import java.awt.Graphics;
import Core.Handler;
import GameObject.Point;
import Utils.WorldTextFileLoader;

public class World {
	private Handler handler;
	private int width, height;
	private Point playerSpawn;
	private Point[] enemySpawn, gateSpawn, runeSpawn;
	private int[] gateType, runeType;
	private Point[][] enemyPatrolRoute;
	private int[][] tilesID;
	private String prevMap, nextMap;
	
	//Constructors
	public World(Handler handler) {
		this.handler = handler;
	}
	
	//Methods
	public Tile getTile(int x, int y) {
		if (x < 0 || y < 0 || x >= width || y >= height) return Tile.grassTile;
		//System.out.println(x + " " + y);
		Tile t = Tile.tiles[tilesID[x][y]];
		if (t == null) return Tile.grassTile;
		return t;
	}
	
	public void loadWorld(String path) {
		int offset = 0;
		String file = WorldTextFileLoader.loadFile(path);
		String[] stringToken = file.split("\\s+");
		
		//Read map
		width = WorldTextFileLoader.parseInt(stringToken[0]);
		height = WorldTextFileLoader.parseInt(stringToken[1]);
		offset = 2;
		
		tilesID = new int[width][height];
		for (int y=0;y<height;y++)
			for (int x=0;x<width;x++){
				tilesID[x][y] = WorldTextFileLoader.parseInt(stringToken[offset+x+y*width]);
			}
		offset += width*height;
		
		//Read player's spawn position
		playerSpawn = new Point(WorldTextFileLoader.parseInt(stringToken[offset]), 
				WorldTextFileLoader.parseInt(stringToken[offset+1]));
		offset += 2;
		
		//Read enemy's spawn position and patrol route
		int numberOfEnemy = WorldTextFileLoader.parseInt(stringToken[offset]);
		int x = 0;
		int y = 0;
		int patrolRouteLength;
		enemySpawn = new Point[numberOfEnemy];
		enemyPatrolRoute = new Point[numberOfEnemy][100];
		offset++;
		for (int i = 0; i < numberOfEnemy; i++) {
			x = WorldTextFileLoader.parseInt(stringToken[offset]);
			y = WorldTextFileLoader.parseInt(stringToken[offset+1]);
			enemySpawn[i] = new Point(x,y);
			offset+=2;
			patrolRouteLength = WorldTextFileLoader.parseInt(stringToken[offset]);
			offset++;
			for (int j = 0; j < patrolRouteLength; j++) {
				x = WorldTextFileLoader.parseInt(stringToken[offset+j*2]);
				y = WorldTextFileLoader.parseInt(stringToken[offset+j*2+1]);
				enemyPatrolRoute[i][j] = new Point(x,y);
			}
			offset+=2*patrolRouteLength;
		}		
		//Read gate
		int numOfGate = WorldTextFileLoader.parseInt(stringToken[offset]);
		offset++;
		gateSpawn = new Point[numOfGate];
		gateType = new int[numOfGate];
		for (int i=0;i<numOfGate;i++) {
			gateType[i] = WorldTextFileLoader.parseInt(stringToken[offset]);
			x = WorldTextFileLoader.parseInt(stringToken[offset+1]);
			y = WorldTextFileLoader.parseInt(stringToken[offset+2]);
			gateSpawn[i] = new Point(x,y);
			offset+=3;
		}		
		//Read rune
		int numOfRune = WorldTextFileLoader.parseInt(stringToken[offset]);
		offset++;
		runeSpawn = new Point[numOfRune];
		runeType = new int[numOfRune];
		for (int i=0;i<numOfRune;i++) {
			runeType[i] = WorldTextFileLoader.parseInt(stringToken[offset]);
			x = WorldTextFileLoader.parseInt(stringToken[offset+1]);
			y = WorldTextFileLoader.parseInt(stringToken[offset+2]);
			runeSpawn[i] = new Point(x,y);
			offset+=3;
		}	
		//Read the address of prev map
		prevMap = stringToken[offset];
		
		nextMap = stringToken[offset+1];
	}
	
	public void update() {
		
	}
	
	public void render(Graphics g) {
		int xStart = Math.max(0, handler.getGame().getGameCamera().getxOffset() / Tile.TILE_WIDTH);
		int xEnd = Math.min(width, (handler.getGame().getGameCamera().getxOffset() + handler.getGame().getWidth()) / Tile.TILE_WIDTH + 1);
		int yStart = Math.max(0, handler.getGame().getGameCamera().getyOffset() / Tile.TILE_HEIGHT);
		int yEnd = Math.min(height, (handler.getGame().getGameCamera().getyOffset() + handler.getGame().getHeight()) / Tile.TILE_HEIGHT + 1);
		
		for (int y=yStart;y<yEnd;y++)
			for (int x=xStart;x<xEnd;x++){
				getTile(x,y).render(g, x * Tile.TILE_WIDTH - handler.getGame().getGameCamera().getxOffset()
							, y * Tile.TILE_HEIGHT - handler.getGame().getGameCamera().getyOffset());
			}
	}
	//Getters and Setters

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public Point getPlayerSpawn() {
		return playerSpawn;
	}

	public Point[] getEnemySpawn() {
		return enemySpawn;
	}

	public Point[][] getEnemyPatrolRoute() {
		return enemyPatrolRoute;
	}

	public String getNextMap() {
		return nextMap;
	}

	public String getPrevMap() {
		return prevMap;
	}

	public Point[] getGateSpawn() {
		return gateSpawn;
	}

	public Point[] getRuneSpawn() {
		return runeSpawn;
	}

	public int[] getGateType() {
		return gateType;
	}

	public int[] getRuneType() {
		return runeType;
	}
	
}
