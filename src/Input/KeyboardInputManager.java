package Input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyboardInputManager implements KeyListener{
	private boolean keys[];
	public boolean up, down, left, right, enter, space, lock;
	
	//Constructors	
	public KeyboardInputManager() {
		keys = new boolean[256];
		lock = false;
	}
	
	//Methods
	public void update() {		
		up = keys[KeyEvent.VK_UP] || keys[KeyEvent.VK_W];
		down = keys[KeyEvent.VK_DOWN] || keys[KeyEvent.VK_S];
		left = keys[KeyEvent.VK_LEFT] || keys[KeyEvent.VK_A];
		right = keys[KeyEvent.VK_RIGHT] || keys[KeyEvent.VK_D];
		enter = keys[KeyEvent.VK_ENTER];
		if (lock) enter = false; 
		if (enter) lock = true;
		space = keys[KeyEvent.VK_SPACE];		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		keys[e.getKeyCode()] = true;
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		keys[e.getKeyCode()] = false;
		if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_SPACE) lock = false;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}
	
	private static KeyboardInputManager inst;
	public static KeyboardInputManager getInst() {
		if (inst == null) 
			inst = new KeyboardInputManager();
		return inst;
	}

}
