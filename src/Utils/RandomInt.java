package Utils;

import java.util.Random;

public class RandomInt {
	public static int generate(int max) {
		Random rand = new Random();
		return rand.nextInt(max);
	}
}
