package Graphics;

import java.awt.image.BufferedImage;

import Utils.ImageLoader;

public class Assets {
	
	private static final int width = 32, height = 32;
	public static BufferedImage[] p_up, p_down, p_left, p_right;
	public static BufferedImage[] orc_up, orc_down, orc_left, orc_right;
	public static BufferedImage grass, rock, dirt, water, forest, gate;
	public static BufferedImage[] but_start, but_quit;
	public static BufferedImage menuscreen, hp_rune, mp_rune, regen_rune, win, lose;
	public static BufferedImage flag;
	
	public static void init(){
		SpriteSheet sheet = new SpriteSheet(ImageLoader.loadImage("/textures/Clown.png"));
		p_up = new BufferedImage[3];
		p_down = new BufferedImage[3];
		p_left = new BufferedImage[3];
		p_right = new BufferedImage[3];
		
		p_up[0] = sheet.crop(0, height * 3, width, height);
		p_up[1] = sheet.crop(width, height * 3, width, height);
		p_up[2] = sheet.crop(width *2, height * 3, width, height);
		
		p_right[0] = sheet.crop(0, height * 2, width, height);
		p_right[1] = sheet.crop(width, height * 2, width, height);
		p_right[2] = sheet.crop(width *2, height * 2, width, height);
		
		p_left[0] = sheet.crop(0, height, width, height);
		p_left[1] = sheet.crop(width, height, width, height);
		p_left[2] = sheet.crop(width *2, height, width, height);
		
		p_down[0] = sheet.crop(0, 0, width, height);
		p_down[1] = sheet.crop(width, 0, width, height);
		p_down[2] = sheet.crop(width *2, 0, width, height);
		
		sheet = new SpriteSheet(ImageLoader.loadImage("/textures/orc.png"));
		orc_up = new BufferedImage[3];
		orc_down = new BufferedImage[3];
		orc_left = new BufferedImage[3];
		orc_right = new BufferedImage[3];
		
		orc_up[0] = sheet.crop(0, height * 3, width, height);
		orc_up[1] = sheet.crop(width, height * 3, width, height);
		orc_up[2] = sheet.crop(width *2, height * 3, width, height);
		
		orc_right[0] = sheet.crop(0, height * 2, width, height);
		orc_right[1] = sheet.crop(width, height * 2, width, height);
		orc_right[2] = sheet.crop(width *2, height * 2, width, height);
		
		orc_left[0] = sheet.crop(0, height, width, height);
		orc_left[1] = sheet.crop(width, height, width, height);
		orc_left[2] = sheet.crop(width *2, height, width, height);
		
		orc_down[0] = sheet.crop(0, 0, width, height);
		orc_down[1] = sheet.crop(width, 0, width, height);
		orc_down[2] = sheet.crop(width *2, 0, width, height);
		
		sheet = new SpriteSheet(ImageLoader.loadImage("/textures/tilesSheet.png"));
		grass = sheet.crop(0, height, width, height);
		rock = sheet.crop(width * 4, height * 2, width, height);
		dirt = sheet.crop(width * 2, height, width, height);
		gate = sheet.crop(width * 4, height * 8, width, height);
		
		but_start = new BufferedImage[2];
		but_quit = new BufferedImage[2];
		sheet = new SpriteSheet(ImageLoader.loadImage("/textures/button.png"));
		but_start[0] = sheet.crop(0, 0, width * 4, height *2);
		but_start[1] = sheet.crop(width * 4, 0, width * 4, height *2);
		but_quit[0] = sheet.crop(0, height * 2 , width * 4, height *2);
		but_quit[1] = sheet.crop(width * 4, height * 2, width * 4, height *2);
		
		sheet = new SpriteSheet(ImageLoader.loadImage("/textures/menuscreen.png"));
		menuscreen = sheet.crop(0, 0, 384, 320);
		sheet = new SpriteSheet(ImageLoader.loadImage("/textures/win.png"));
		win = sheet.crop(0, 0, 384, 320);
		sheet = new SpriteSheet(ImageLoader.loadImage("/textures/lose.png"));
		lose = sheet.crop(0, 0, 384, 320);
		sheet = new SpriteSheet(ImageLoader.loadImage("/textures/flag.png"));
		flag = sheet.crop(0, 0, 32, 32);
		
		sheet = new SpriteSheet(ImageLoader.loadImage("/textures/skillitem.png"));
		hp_rune = sheet.crop(4 * width, 6 * height, width, height);
		mp_rune = sheet.crop(5 * width, 6 * height, width, height);
		regen_rune = sheet.crop(6 * width, 6 * height, width, height);

	}
}
