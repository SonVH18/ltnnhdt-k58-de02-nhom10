package Graphics;

import Core.Handler;
import GameObject.GameObject;
import World.Tile;

public class GameCamera {
	
	private int xOffset, yOffset;
	private Handler handler;
	
	public GameCamera(Handler handler, int xOffset, int yOffset) {
		this.handler = handler;
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}
	

	private void checkCameraLimit() {
		if (xOffset < 0) xOffset = 0;
		else if (xOffset > handler.getWorld().getWidth() * Tile.TILE_WIDTH - handler.getGame().getWidth())
			xOffset = handler.getWorld().getWidth() * Tile.TILE_WIDTH - handler.getGame().getWidth();
		if (yOffset < 0) yOffset = 0;
		else if (yOffset > handler.getWorld().getHeight() * Tile.TILE_HEIGHT - handler.getGame().getHeight())
			yOffset = handler.getWorld().getHeight() * Tile.TILE_HEIGHT - handler.getGame().getHeight();
	}
	
	public void moveCamera(int xAmount, int yAmount) {
		xOffset += xAmount;
		yOffset += yAmount;
		checkCameraLimit();
	}
		
	public void centerOnGameObject(GameObject object) {
		xOffset = object.getX() + (object.getWidth() - handler.getGame().getWidth())/ 2;
		yOffset = object.getY() + (object.getHeight() - handler.getGame().getHeight()) / 2;
		checkCameraLimit();
	}
	//Getters and Setters

	public int getxOffset() {
		return xOffset;
	}

	public void setxOffset(int xOffset) {
		this.xOffset = xOffset;
	}

	public int getyOffset() {
		return yOffset;
	}

	public void setyOffset(int yOffset) {
		this.yOffset = yOffset;
	}
	
}
