package UI;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class QuitButton extends UIObject{

	private BufferedImage[] images;
	private ClickListener clicker;
	
	//Constructors
	public QuitButton(int x, int y, int width, int height, BufferedImage[] images, ClickListener clicker) {
		super(x, y, width, height);
		this.images = images;
		this.clicker = clicker;
	}

	//Methods
	@Override
	public void update() {
		
	}

	@Override
	public void render(Graphics g) {
		if(hovering)
			g.drawImage(images[1], x, y, width, height, null);
		else
			g.drawImage(images[0], x, y, width, height, null);
	}

	@Override
	public void onClick() {
		clicker.onClick();
	}

}
