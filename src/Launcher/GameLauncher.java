package Launcher;

import Core.Game;

public class GameLauncher {
	public static void main(String[] args) {
		Game game = new Game("RPG!", 384, 320);
		game.start();
	}
}
