package Core;

import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import Core.State.BattleState;
import Core.State.EndState;
import Core.State.GameState;
import Core.State.MenuState;
import Core.State.State;
import Core.State.StateManager;
import Graphics.Assets;
import Graphics.GameCamera;
import Input.KeyboardInputManager;
import Input.MouseInputManager;

public class Game implements Runnable{
	public static final int FRAME_PER_SECOND = 60;
	
	private Display display;
	private int width, height;
	private String title;
	
	private Thread gameThread;
	private boolean isRunning = false;	

	private BufferStrategy bufferStrategy;
	private Graphics g;
	
	private Handler handler;
	
	private KeyboardInputManager keyInput;
	private MouseInputManager mouseInput;
	
	private GameCamera gameCamera;
	
	public State menuState, gameState, battleState, endState;
	
	//Constructors
	public Game(String title, int width, int height) {
		this.width = width;
		this.height = height;
		this.title = title;		
		keyInput = new KeyboardInputManager();
		mouseInput = new MouseInputManager();
	}
	
	//Methods
	private void init() {
		display = new Display(title, width, height);
		display.getFrame().addKeyListener(keyInput);
		display.getFrame().addMouseListener(mouseInput);
		display.getFrame().addMouseMotionListener(mouseInput);
		display.getCanvas().addMouseListener(mouseInput);
		display.getCanvas().addMouseMotionListener(mouseInput);
		Assets.init();
		handler = new Handler(this);
		gameCamera = new GameCamera(handler, 50, 50);
		menuState = new MenuState(handler);
		gameState = new GameState(handler);
		battleState = new BattleState(handler, null, null);
		endState = new EndState(handler);
		StateManager.setState(menuState);
	}
	
	private void update() {
		keyInput.update();
		if (StateManager.getState() != null) 
			StateManager.getState().update();
	}
	
	private void render() {
		bufferStrategy = display.getCanvas().getBufferStrategy();
		if (bufferStrategy == null) {
			display.getCanvas().createBufferStrategy(3);
			return;
		}
		g = bufferStrategy.getDrawGraphics();
		//Clear Screen
		g.clearRect(0, 0, width, height);
		//Draw
		if (StateManager.getState() != null) 
			StateManager.getState().render(g);
		//After Draw
		bufferStrategy.show();
		g.dispose();
		
	}

	@Override
	public void run() {
		init();
		
		double timePerUpdate = 1000000000 / FRAME_PER_SECOND;
		double delta = 0;
		long now = 0;
		long lastTime = System.nanoTime();
		
		while(isRunning) {
			now = System.nanoTime();
			delta += (now - lastTime) / timePerUpdate;
			lastTime = now;
			if (delta >= 1) {
				update();
				render();
				delta--;
			}	
		}
		
		stop();
	}
	
	public synchronized void start() {
		if (isRunning) return;
		gameThread = new Thread(this);
		gameThread.start();
		isRunning = true;
	}
	
	public synchronized void stop() {
		if (!isRunning) return;
		try {
			gameThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		isRunning = false;
	}
	//Getters and Setters
	
	public KeyboardInputManager getKeyInput() {
		return keyInput;
	}
	
	public MouseInputManager getMouseInput() {
		return mouseInput;
	}
	
	public GameCamera getGameCamera() {
		return gameCamera;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
}
