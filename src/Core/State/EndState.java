package Core.State;

import java.awt.Graphics;

import Core.Handler;
import Graphics.Assets;

public class EndState extends State{
	
	private boolean isVictorious;

	//Constructors
	public EndState(Handler handler) {
		super(handler);
	}

	//Methods
	@Override
	public void update() {
		if (handler.getGame().getKeyInput().enter) 
			StateManager.setState(handler.getGame().menuState);
	}

	@Override
	public void render(Graphics g) {
		if (isVictorious) 
			g.drawImage(Assets.win, 0, 0, null);
		else g.drawImage(Assets.lose, 0, 0, null);
	}

	//Getters and Setters
	public boolean isVictorious() {
		return isVictorious;
	}

	public void setVictorious(boolean isVictorious) {
		this.isVictorious = isVictorious;
	}

}
