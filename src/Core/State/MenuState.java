package Core.State;

import java.awt.Graphics;

import Core.Handler;
import Graphics.Assets;
import UI.ClickListener;
import UI.QuitButton;
import UI.StartButton;
import UI.UIObjectManager;


public class MenuState extends State{

	private UIObjectManager uiManager;
	
	//Constructors
	public MenuState(Handler handler) {
		super(handler);
		uiManager = new UIObjectManager(handler);
		handler.getMouseInput().setUiManager(uiManager);
		
		uiManager.addObject(new StartButton(144, 192, 96, 48, Assets.but_start, 
				new ClickListener(){

				@Override
				public void onClick() {
					GameState gs = (GameState) handler.getGame().gameState;
					gs.reset();
					StateManager.setState(handler.getGame().gameState);
				}
			}));
		
		uiManager.addObject(new QuitButton(144, 240, 96, 48, Assets.but_quit, 
				new ClickListener(){

				@Override
				public void onClick() {
					handler.getMouseInput().setUiManager(null);
					System.exit(0);
				}
			}));

	}

	//Methods
	@Override
	public void update() {
		uiManager.update();
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(Assets.menuscreen, 0, 0, 384, 320, null);
		uiManager.render(g);
	}

}
