package Core.State;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;

import Core.Handler;
import GameObject.Enemy;
import GameObject.Player;
import GameObject.Skill;

public class BattleState extends State {
	private static final int HP_BAR_LENGTH = 100;

	private Player player;
	private Enemy enemy;
	private int choice;
	private ArrayList<String> messages;
	//Constructors
	public BattleState(Handler handler, Player player, Enemy enemy) {
		super(handler);
		setPlayer(player);
		setEnemy(enemy);
		choice = 7;
		messages = new ArrayList<String>();
	}

	//Methods
	public void clearMsg() {
		messages.clear();		
	}
	
	private int getChoice() {
		return choice / 7 - 1;
	}

	private void battle() {
		messages.clear();
		int damage;
		Skill skill = player.getSkills()[getChoice()];
		switch (skill.getSkillType()) {
		case Skill.NUKE:
			messages.add("You use Magical Damage");
			damage = skill.getDamage() - enemy.getDef();
			if (player.getMP() >= skill.getManaCost()) {
				player.drainMana(skill.getManaCost());
				enemy.takeDamage(damage);
				messages.add("Enemy take " + damage + " damage!");
			} else 
				messages.add("Not enough mana, nothing happen!");
			break;
		case Skill.PHYSICAL:
			messages.add("You use Physical Damage");
			damage = player.getAtk() - enemy.getDef();
			messages.add("Enemy take " + damage + " damage!");
			enemy.takeDamage(damage);
			if (player.isBuffAtk()) {
				player.setBuffAtk(false);
				player.setAtk(player.getNormalAtk());
				messages.add("ATK Buff lost!");
			}
			break;
		case Skill.BUFF_ATTACK:
			messages.add("You use ATK buff");
			if (player.getMP() >= skill.getManaCost()) {
				player.setBuffAtk(true);
				player.setAtk(player.getAtk() + skill.getDamage());
				player.drainMana(skill.getManaCost());
				messages.add("ATK increased " + skill.getDamage() + " point!");
			} else 
				messages.add("Not enough mana, nothing happen!");
			break;
		case Skill.BUFF_DEFENSE:
			messages.add("You use DEF buff");
			if (player.getMP() >= skill.getManaCost()) {
				player.setBuffDef(true);
				player.setDef(player.getDef() + skill.getDamage());
				player.drainMana(skill.getManaCost());
				messages.add("DEF increased " + skill.getDamage() + " point!");
			} else 
				messages.add("Not enough mana, nothing happen!");
			break;
		default:
			break;
		}
		if (!enemy.getDeadStatus()) {
			damage = enemy.getAtk() - player.getDef();
			if (damage < 0) damage = 0;
			player.takeDamage(damage);			
			messages.add("Enemy counterattack!");
			messages.add("You take " + damage + " damage!");
			if (player.isBuffDef()) {
				player.setBuffDef(false);
				player.setDef(player.getNormalDef());
				messages.add("DEF Buff lost!");
			}
			if (player.getDeadStatus()) {
				EndState es = (EndState) handler.getGame().endState;
				es.setVictorious(false);
				messages.add("Enemy killed you!");
				messages.add("Press Space to quit!");
			}
		} else {
			messages.add("You killed enemy!");
			messages.add("Press Space to continue!");
		}
	}

	private void upOneChoice() {
		if (choice > 7)
			choice--;
	}

	private void downOneChoice() {
		if (choice < 28)
			choice++;
	}

	@Override
	public void update() {
		player.animUpdate();
		enemy.animUpdate();
		if (handler.getGame().getKeyInput().down)
			downOneChoice();
		else if (handler.getGame().getKeyInput().up)
			upOneChoice();
		if (!player.getDeadStatus() && !enemy.getDeadStatus()) {
			if (handler.getGame().getKeyInput().enter)
				battle();
		} else {
			if (enemy.getDeadStatus()) 
				if (handler.getGame().getKeyInput().space)
					StateManager.setState(handler.getGame().gameState);
			if (player.getDeadStatus()) 
				if (handler.getGame().getKeyInput().space)
					StateManager.setState(handler.getGame().endState);
		}
	}

	@Override
	public void render(Graphics g) {
		
		// Draw player's HP bar
		int hpLength;
		int mpLength;
		g.setColor(Color.RED);
		hpLength = player.getHP() * HP_BAR_LENGTH / player.getMaxHP();
		g.fillRect(10, 10, hpLength, 5);
		g.setColor(Color.BLACK);
		g.fillRect(10 + hpLength, 10, HP_BAR_LENGTH - hpLength, 5);
		g.drawString(player.getHP() + "/" + player.getMaxHP(), 11 + HP_BAR_LENGTH, 17);
		
		//Draw player's MP bar
		g.setColor(Color.BLUE);
		mpLength = player.getMP() * HP_BAR_LENGTH / player.getMaxMP();
		g.fillRect(10, 25, mpLength, 5);
		g.setColor(Color.BLACK);
		g.fillRect(10 + mpLength, 25, HP_BAR_LENGTH - mpLength, 5);
		g.drawString(player.getMP() + "/" + player.getMaxMP(), 11 + HP_BAR_LENGTH, 32);
		g.drawString("ATK: " + player.getAtk(), 11 + HP_BAR_LENGTH, 47);
		g.drawString("DEF: " + player.getDef(), 11 + HP_BAR_LENGTH, 62);

		//Draw enemy's HP bar
		g.setColor(Color.RED);
		hpLength = enemy.getHP() * HP_BAR_LENGTH / enemy.getMaxHP();
		g.fillRect(230, 250, hpLength, 5);
		g.setColor(Color.BLACK);
		g.fillRect(230 + hpLength, 250, HP_BAR_LENGTH - hpLength, 5);
		g.drawString(enemy.getHP() + "/" + enemy.getMaxHP(), 231 + HP_BAR_LENGTH, 257);
		g.drawString("ATK: " + enemy.getAtk(), 231 + HP_BAR_LENGTH, 272);
		g.drawString("DEF: " + enemy.getDef(), 231 + HP_BAR_LENGTH, 287);
		
		// Draw skill
		g.drawRect(170, 10, 210, 70);
		g.setFont(new Font("Courier New", Font.PLAIN, 15));
		g.drawString("Physical Attack(0)", 210, 25);
		g.drawString("Magical Attack(200)", 210, 40);
		g.drawString("ATK Buff(100)", 210, 55);
		g.drawString("DEF Buff(100)", 210, 70);
		g.drawString("==>", 180, 25 + 15 * (choice / 7 - 1));
		for (int i = 0; i < messages.size(); i++) {
			g.drawString(messages.get(i), 10, 200 + i * 15);
		}
		//Draw Image
		g.drawImage(player.getBattleAnimationFrame(), 133, 118, 64, 64, null);
		g.drawImage(enemy.getBattleAnimationFrame(), 203, 118, 64, 64, null);

	}

	//Getters and Setters

	public void setPlayer(Player player) {
		this.player = player;
	}

	public void setEnemy(Enemy enemy) {
		this.enemy = enemy;
	}

}
