package Core.State;

import java.awt.Graphics;

import Core.Handler;

public abstract class State {
	protected Handler handler;
	public State(Handler handler) {
		this.handler = handler;
	}
	public abstract void update();
	public abstract void render(Graphics g);
}
