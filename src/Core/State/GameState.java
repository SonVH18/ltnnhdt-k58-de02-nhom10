package Core.State;

import java.awt.Color;
import java.awt.Graphics;

import Core.Handler;
import GameObject.Enemy;
import GameObject.GameObjectManager;
import GameObject.Gate;
import GameObject.Player;
import GameObject.Rune;
import World.World;

public class GameState extends State{
	private Player player;
	private Enemy[] enemy;
	private Gate[] gate;
	private Rune[] rune;
	private World world;	
	
	//Constructors
	public GameState(Handler handler) {
		super(handler);
		reset();
	}
	
	//Methods
	public void reset() {
		world = new World(handler);
		loadWorld("res/world/world1.txt");
		player.resetStat();
	}

	public void loadWorld(String path) {
		world.loadWorld(path);
		//Init player
		int spawnX = world.getPlayerSpawn().getX();
		int spawnY = world.getPlayerSpawn().getY();
		player = Player.getInst(handler);
		player.setX(spawnX);
		player.setY(spawnY);
		//Init Enemy
		enemy = new Enemy[world.getEnemySpawn().length];
		for (int i = 0; i < world.getEnemySpawn().length; i++) {
			enemy[i] = new Enemy(handler, world.getEnemySpawn()[i].getX(),
					world.getEnemySpawn()[i].getY(), world.getEnemyPatrolRoute()[i]);
		}
		gate = new Gate[world.getGateSpawn().length];
		for (int i = 0; i < gate.length; i++) {
			gate[i] = new Gate(handler, world.getGateType()[i], 
					world.getGateSpawn()[i].getX(), world.getGateSpawn()[i].getY());
		}
		rune = new Rune[world.getRuneSpawn().length];
		for (int i = 0; i < rune.length; i++) {
			rune[i] = new Rune(handler, world.getRuneType()[i], 
					world.getRuneSpawn()[i].getX(), world.getRuneSpawn()[i].getY());
		}
		GameObjectManager gameObjectManager = GameObjectManager.getInst(handler);
		gameObjectManager.clear();
		handler.setWorld(world);
		gameObjectManager.addPlayer(player);
		gameObjectManager.addEnemy(enemy);
		gameObjectManager.addGate(gate);
		gameObjectManager.addRune(rune);
		handler.setGameObjectManager(gameObjectManager);
	}

	
	@Override
	public void update() {
		world.update();
		player.update();
		int checkGate = player.checkGateCollision();
		if (checkGate == Gate.NEXT_MAP_GATE) loadWorld(world.getNextMap());
		if (checkGate == Gate.PREV_MAP_GATE) loadWorld(world.getPrevMap());
		if (checkGate == Gate.END_GAME_GATE) {
			EndState es = (EndState) handler.getGame().endState;
			es.setVictorious(true);
			StateManager.setState(handler.getGame().endState);			
		}
		
		Rune eatenRune = player.checkRuneCollision();
		if (eatenRune != null) player.eatRune(eatenRune);
		Enemy e_ = player.checkEnemyCollision(0, 0);
		if (e_ != null) {
			BattleState battleState = (BattleState) handler.getGame().battleState;
			battleState.setEnemy(e_);
			battleState.setPlayer(player);
			battleState.clearMsg();
			StateManager.setState(battleState);
		}
		for (Enemy e : enemy) if (!e.getDeadStatus()) e.update();
		for (Gate g_ : gate) g_.update(); 
		for (Rune r : rune) if (!r.isEaten()) r.update();
	}

	@Override
	public void render(Graphics g) {
		world.render(g);
		player.render(g);
		for (Enemy e : enemy) if (!e.getDeadStatus()) e.render(g);
		for (Gate g_ : gate) g_.render(g); 
		for (Rune r : rune) if (!r.isEaten()) r.render(g);
		g.setColor(Color.WHITE);
		g.drawString("HP: "+player.getHP() + "/" + player.getMaxHP(), 10, 17);
		g.drawString("MP: "+player.getMP() + "/" + player.getMaxMP(), 10, 17+15);
	}
}
