package Core;

import GameObject.GameObjectManager;
import Input.MouseInputManager;
import World.World;

public class Handler {
	private Game game;
	private World world;
	private GameObjectManager gameObjectManager;
	
	//Constructors
	public Handler(Game game) {
		this.game = game;
	}
	
	//Getters and Setters
	public Game getGame() {
		return game;
	}

	public World getWorld() {
		return world;
	}

	public void setWorld(World world) {
		this.world = world;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public GameObjectManager getGameObjectManager() {
		return gameObjectManager;
	}

	public void setGameObjectManager(GameObjectManager gameObjectManager) {
		this.gameObjectManager = gameObjectManager;
	}	
	
	public MouseInputManager getMouseInput(){
		return game.getMouseInput();
	}
}
