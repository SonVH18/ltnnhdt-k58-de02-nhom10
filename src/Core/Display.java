package Core;

import java.awt.Canvas;
import java.awt.Dimension;

import javax.swing.JFrame;

public class Display{
	private JFrame mainGameFrame;
	private Canvas canvas;
	
	private String title;
	private int width, height;
	
	//Constructor
	public Display(String title, int width, int height){
		this.title = title;
		this.width = width;
		this.height = height;
		createDisplay();
	}

	//Methods
	private void createDisplay() {
		//Create main game frame
		mainGameFrame = new JFrame();
		mainGameFrame.setTitle(title);
		mainGameFrame.setSize(width, height);
		mainGameFrame.setVisible(true);
		mainGameFrame.setResizable(false);
		mainGameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainGameFrame.setLocationRelativeTo(null);
		//Create Canvas
		canvas = new Canvas();
		canvas.setPreferredSize(new Dimension(width, height));
		canvas.setMaximumSize(new Dimension(width, height));
		canvas.setMinimumSize(new Dimension(width, height));
		canvas.setFocusable(false);
		
		mainGameFrame.add(canvas);
		mainGameFrame.pack();
	}
	
	//Getters and Setters
	public Canvas getCanvas() {
		return canvas;
	}
	
	public JFrame getFrame() {
		return mainGameFrame;
	}
}
